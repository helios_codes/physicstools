#include "ensdf_decoder.hpp"

using namespace std;

/////////////////////////////////////////////////////////////////////////////
// Constructor
/////////////////////////////////////////////////////////////////////////////
ensdf_decoder::ensdf_decoder()  
{
  goodDataFile = 0;
  numLines = 0;
  cout << "ensdf decoder" << endl;
}

/////////////////////////////////////////////////////////////////////////////
// Public method that reads an ENSDF text file and saves each line in an 
// array (ensdfLine) from which data will be extracted. It returns 0 if the
// file was not found and 1 otherwise.
/////////////////////////////////////////////////////////////////////////////
int ensdf_decoder::loadDatabase(string File)
{
  ifstream read;
  string line;
  goodDataFile = 0;
  numLines = 0;
  
  read.open(File.c_str());
  if(!read.is_open()) 
    cout << "*** ENSDF file " << File << " was not found." << endl;
  else {
    // The file is now open. read it and count the lines.
    goodDataFile = 1;
    do{
      getline(read, line);
      numLines++; 
    } while (!read.eof());
    read.close();
    
    ensdfLine = new string[numLines];
    
    // Reopen the file to read and save the data in memory.
    read.open(File.c_str());
    // First line is for colum description.
    for (int n=0; n<numLines; n++) {
      getline(read, line);
      ensdfLine[n] = line;
    }
    read.close();
    cout << "ENSDF file \"" << File << "\" read with " << numLines << " lines" << endl;
  }

  return goodDataFile;
}


/////////////////////////////////////////////////////////////////////////////
//
/////////////////////////////////////////////////////////////////////////////
int ensdf_decoder::dumpStableNuclei(string file)
{
  ofstream write(file.c_str());
  string gsKey1 = "  L 0";
  string gsKey2 = "  L  0";
  string gsKey3 = "  L   0";
  string gsKey4 = "  L    0";
  string gsKey5 = "  L     0";
  string gsKey6 = "  L      0";
  string gsKey7 = "  L       0";
  string gsKey8 = "  L        0";
  string gsKey9 = "  L         0";
  string gsKeyA = "  L          0";

  string nuclide;
  for (int i=0; i<numLines; i++) {
    if (ensdfLine[i].find("STABLE")!=string::npos) {
      if (ensdfLine[i].find(gsKey1)!=string::npos || ensdfLine[i].find(gsKey2)!=string::npos ||
	  ensdfLine[i].find(gsKey3)!=string::npos || ensdfLine[i].find(gsKey4)!=string::npos ||
	  ensdfLine[i].find(gsKey5)!=string::npos || ensdfLine[i].find(gsKey6)!=string::npos ||
	  ensdfLine[i].find(gsKey7)!=string::npos || ensdfLine[i].find(gsKey8)!=string::npos ||
	  ensdfLine[i].find(gsKey9)!=string::npos || ensdfLine[i].find(gsKeyA)!=string::npos) {
	cout << ensdfLine[i] << " ";
	nuclide = ensdfLine[i].substr(0,5);
	string str = ensdfLine[i].substr(4,1);
	char* letter = new char[str.length() + 1];
	strcpy(letter, str.c_str());
	if(letter[0] <= 'Z' && letter[0] >= 'A')
	  letter[0] = letter[0] - ('Z' - 'z');
	nuclide.replace(4,1,1,letter[0]);
	string::iterator end_pos = remove(nuclide.begin(), nuclide.end(), ' ');
	nuclide.erase(end_pos, nuclide.end());
	cout << letter[0] << " |" << nuclide << "|"<< endl;
	write << nuclide << endl;
      }
    }
  }
  write.close();

  return 0;
}
