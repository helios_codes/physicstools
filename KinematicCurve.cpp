/*******************************************************************
Code: KinematicCurve.cpp

Description: This class gives the kinematic curves as a TGraph
  object. The masses for the reaction and the energy of excitation
  must be set before getting the curve.

Author: Daniel Santiago-Gonzalez
2013-March
*******************************************************************/

// Compile with: 
// g++ -shared -fPIC KinematicCurve.cpp FourVector.so `root-config --cflags --glibs` -o KinematicCurve.so
// Not working with ROOT (CINT)!

#include "KinematicCurve.hpp"

using namespace std;

//////////////////////////////////////////////////////////////////////////////////////////
// Constructor
//////////////////////////////////////////////////////////////////////////////////////////
KinematicCurve::KinematicCurve()
{

}


//////////////////////////////////////////////////////////////////////////////////////////
// Version of SetReaction with float arguments
//////////////////////////////////////////////////////////////////////////////////////////
void KinematicCurve::SetReaction(float Kb, float mb, float mt, float mh, float ml, float Eexc) 
{
  SetReaction((double)Kb, (double)mb, (double)mt, (double)mh, (double)ml, (double)Eexc);
  return;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Establish the reaction conditions
//////////////////////////////////////////////////////////////////////////////////////////
void KinematicCurve::SetReaction(double Kb, double mb, double mt, double mh, double ml, double Eexc)
{
  // All masses must be in MeV/c^2 and energies in MeV.
  this->Kb = Kb;     // beam kinetic energy.
  this->mb = mb;     // beam mass
  this->mt = mt;     // target mass
  this->mh = mh;     // heavy ion mass
  this->ml = ml;     // light ion mass
  this->Eexc = Eexc; // excitation energy of heavy ion.
  Pb.SetName("Pb");
  Pt.SetName("Pt");
  Pl.SetName("Pl");
  Ph.SetName("Ph");
  Ptot.SetName("Ptot");
  pb = sqrt(2*mb*Kb*(1 + Kb/(2*mb)));
  Pb.SetCoords(mb+Kb,0,0,pb);
  Pt.SetCoords(mt,0,0,0);
  Ptot = Pb + Pt;
  // Center of mass beta (v/c)
  BetaZ = pb/(mb+Kb+mt);

  vb = sqrt(2*Kb/mb);
  vCM = vb*mb/(mt+mb);
  return;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Non-relativistic method for calculating the kinematic curve
//////////////////////////////////////////////////////////////////////////////////////////
void KinematicCurve::GetCurve(int Points, string YvX)
{
  double K_l_lab=0, K_l_CM=0, Q=0, Q0=0, theta_l_CM=0, theta_l_lab=0;
  double v_l_CM=0, v_l_lab=0;

  Q0 = mb + mt - mh - ml;
  Q = Q0 - Eexc;
  K_l_CM = mh*(Q + Kb*(1 - mb/(mh+ml)))/(mh+ml);
  v_l_CM = sqrt(2*K_l_CM/ml);
  
  for(Int_t p=0; p<Points; p++){
    theta_l_CM = pi*p/Points;
    theta_l_lab = atan(v_l_CM*sin(theta_l_CM)/(v_l_CM*cos(theta_l_CM) + vCM));
    if (theta_l_lab<0)
      theta_l_lab += pi;

    if (YvX=="AvA") 
      this->SetPoint(p, theta_l_CM*180/pi, theta_l_lab*180/pi);
    else if (YvX=="EvA") {
      v_l_lab = sqrt(pow(v_l_CM*cos(theta_l_CM)+vCM,2) + pow(v_l_CM*sin(theta_l_CM),2));
      K_l_lab = ml*pow(v_l_lab,2)/2;
      this->SetPoint(p, theta_l_lab*180/pi, K_l_lab);
    }
    else 
      cout << "Warning: invalid YvX option (use EvA or AvA)" << endl;
  }// end for(p)    
  
  return;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Relativistic version of the GetCurve() method that uses four-vectors.
// Tested with 12C(14N,d)24Mg reaction with 14N beam at 30
// MeV. Consistent results with GetCurve() method. More tests would be good.
//////////////////////////////////////////////////////////////////////////////////////////
void KinematicCurve::GetCurveRel(int Points, string YvX)
{
  double Kl=0, theta_CM=0, theta_l_deg=0, pf_CM=0;
  for(Int_t p=0; p<Points; p++){
    theta_CM = pi*p/(Points-1);
    if (Ptot*Ptot>pow(ml+mh+Eexc,2)) {
      // Final momentum in the CM reference frame (Ptot*Ptot is an invariant quantity).
      pf_CM = sqrt((Ptot*Ptot-pow(ml+mh+Eexc,2))*(Ptot*Ptot-pow(mh+Eexc-ml,2))/(4*(Ptot*Ptot)));
      // Set the four-momentum components of the light particle in the center of mass.
      Pl.SetCoords(sqrt(ml*ml + pf_CM*pf_CM), pf_CM*sin(theta_CM),0,-pf_CM*cos(theta_CM));
      // Do a Lorentz transformation (boost) into the lab reference frame.
      // I've double checked the sign of the boost and is correct (-Beta).
      Pl.Boost(0,0,-BetaZ);
      theta_l_deg = Pl.GetTheta()*180/pi;
      Kl = Pl.GetX0() - ml;  // assuming no excitation energy for the light particle
      if (YvX=="EvA")
       	this->SetPoint(p, theta_l_deg, Kl);
      else if (YvX=="AvA") 
	this->SetPoint(p, theta_CM*180/pi, theta_l_deg);
      else
	cout << "Warning: invalid YvX option (use EvA or AvA)" << endl;
    }
  }// end for(p)    

  return;
}


//////////////////////////////////////////////////////////////////////////////////////////
// Relativistic version of the GetCurve() method that uses four-vectors.
// Tested with 12C(14N,d)24Mg reaction with 14N beam at 30
// MeV. Consistent results with GetCurve() method. More tests would be good.
//////////////////////////////////////////////////////////////////////////////////////////
void KinematicCurve::GetCurveRelH(int Points, string YvX)
{
  double K=0, theta_CM=0, theta_lab_deg=0, pf_CM=0;
  for(Int_t p=0; p<Points; p++){
    theta_CM = pi*p/(Points-1);
    if (Ptot*Ptot>pow(ml+mh+Eexc,2)) {
      // Final momentum in the CM reference frame (Ptot*Ptot is an invariant quantity).
      pf_CM = sqrt((Ptot*Ptot-pow(ml+mh+Eexc,2))*(Ptot*Ptot-pow(mh+Eexc-ml,2))/(4*(Ptot*Ptot)));
      // Set the four-momentum components of the heavy particle in the center of mass.
      Ph.SetCoords(sqrt((mh+Eexc)*(mh+Eexc)+pf_CM*pf_CM), -pf_CM*sin(theta_CM),0,pf_CM*cos(theta_CM));
      // Do a Lorentz transformation (boost) into the lab reference frame.
      Ph.Boost(0,0,-BetaZ);
      theta_lab_deg = Ph.GetTheta()*180/pi;
      K = Ph.GetX0() - mh - Eexc;  
      if (YvX=="EvA")
       	this->SetPoint(p, theta_lab_deg, K);
      else if (YvX=="AvA") 
	this->SetPoint(p, theta_CM*180/pi, theta_lab_deg); 
      else
	cout << "Warning: invalid YvX option (use EvA or AvA)" << endl; 
    }
  }// end for(p)    
  
  return;
}


//////////////////////////////////////////////////////////////////////////////////////////
// HELIOS-like kinematic curve
// Ref: NIM Wousmaa 2007
//////////////////////////////////////////////////////////////////////////////////////////
void KinematicCurve::GetHELIOSCurve(int Points, double BField, double zmin, double zmax, int A, int Z)
{
  double K_l_lab=0, K_l_CM=0, Q=0, Q0=0, theta_l_CM=0, theta_l_lab=0;
  double v_l_CM=0, v_l_lab=0;

  if (Points<=1) {
    cout << "Error: number of points must be greater than 1." << endl;
    return;
  }

  Q0 = mb + mt - mh - ml;
  Q = Q0 - Eexc;
  K_l_CM = mh*(Q + Kb*(1 - mb/(mh+ml)))/(mh+ml);
  v_l_CM = sqrt(2*K_l_CM/ml);
  double Tcyc = 65.6*A/Z/BField;  // in ns
  for(Int_t p=0; p<Points; p++){
    double z = zmin + p*(zmax-zmin)/(Points-1);
    double K_l_lab = K_l_CM + ml*vCM*vCM/2/c/c + ml*vCM*z/Tcyc/c/c;
    cout << p << " " << z << " " << K_l_lab << endl;
    this->SetPoint(p, z, K_l_lab);
  }// end for(p)    
  
  return;
}


//////////////////////////////////////////////////////////////////////////////////////////
//
//////////////////////////////////////////////////////////////////////////////////////////
double KinematicCurve::GetCMKineticEnergy()
{
  double pb = sqrt(2*mb*Kb*(1 + Kb/(2*mb)));
  double p_CM = sqrt((Ptot*Ptot - pow(mb+mt,2))*(Ptot*Ptot - pow(mb-mt,2)))/(2*sqrt(Ptot*Ptot));
  double Kb_CM = sqrt(mb*mb + p_CM*p_CM) - mb;
  double Kt_CM = sqrt(mt*mt + p_CM*p_CM) - mt;
  return (Kb_CM + Kt_CM); 
}
