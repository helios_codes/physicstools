/*******************************************************************
Code: NuclideFinder

Description: Portable class from which one can obtain the mass of
             a nuclide by specifying its symbol or 'name', e.g
             "4He". The user must choose the mass units from the
             following string options: "MeV/c^2", "u" (unified atomic 
             mass unit) or "micro-u".  Similarly, the mass and atomic
             numbers can be retrieved from the nuclide 'name'.
             
Compile with: 
* Original
g++ -shared -fPIC NuclideFinder.cpp -o NuclideFinder.so
* Better compatibility results (works with CStoSF and SFtoCS)
g++ -c -o NuclideFinder.so NuclideFinder.cpp

Author: Daniel Santiago-Gonzalez
2015-01
 
*******************************************************************/
#ifndef NuclideFinder_hpp_INCLUDED   
#define NuclideFinder_hpp_INCLUDED   

#include <iostream>
#include <string>

class NuclideFinder {

public:

  NuclideFinder();
  int GetA(int Index);
  int GetArraySize();
  double GetMass(int Index, std::string Units);
  double GetMass(int Z, int A, std::string Units);
  double GetMass(std::string Nuclide, std::string Units);
  int GetN(std::string Nuclide);
  std::string GetName(int Index);
  std::string GetName(int Z, int A);
  int GetZ(int Index);
  int GetZ(std::string Nuclide);

private:
  std::string GetProperName(std::string Nuclide);
  void Init();

  int Size;
  int* A;
  int* N;
  int* Z;
  std::string* Name;
  double* Mass;  // in micro-u (unified atomic mass unit).

  // ClassDef(NuclideFinder,1);

};

#endif
