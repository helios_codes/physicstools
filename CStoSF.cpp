// Code: CStoSF.cpp
//
// Description: Calculates the cross section for the astrophysical S-factor.
// Usage example: Say you want to get the S-factor for the 17O(p,gamma) reaction and you know 
//                that the cross section is 1e-3 barn and that the center-of-mass energy is 0.5 MeV
// CStoSF -c 1e-3 -e 0.5 -b 17O -t p
//
// Compile with:
// g++ -Wall -c CStoSF.cpp
// g++ CStoSF.o NuclideFinder.so -o CStoSF
//
// By Daniel Santiago-Gonzalez
// Aug/2016

#include <iostream>
#include <string>
#include <cmath>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "NuclideFinder.hpp"

using namespace std;

int main (int argc, char** argv)
{
  const double alpha = 1/137.035999139; // fine structure constant
  const double pi = 3.14159265358979323846;

  int opt;
  double CSvalue = 0;
  double Ecm = 0;
  string beam;
  string target;
  int quiet = 0;

  NuclideFinder* NuF = new NuclideFinder();

  opterr = 0;
  while ((opt = getopt (argc, argv, "c:e:b:t:q")) != -1)
    switch (opt) {
    case 'c': // cross section
      CSvalue = atof(optarg);    
      break;
    case 'e': // center-of-mass energy
      Ecm = atof(optarg);
      break;
    case 'b': // beam
      beam = optarg;
      break;
    case 't': // target
      target = optarg;
      break;
    case 'q': // quiet, just output S-factor
      quiet = 1;
      break;
    default:
      fprintf(stderr, "Usage: [-t nsecs] [-n] name\n");
      abort ();
    }
 
  if (!quiet) {
    cout << " Program CStoSF\n"
	 << " by Daniel SG, Aug/2016\n"
	 << " Description: Converts cross section to astrophysical S-factor.\n"
	 << " Usage example: Say you want the S-factor for the 17O(p,gamma) reaction and you know\n"
	 << " that the cross section is 1e-3 barn at a center-of-mass energy of 0.5 MeV, type\n"
	 << " CStoSF -c 1e-3 -e 0.5 -b 17O -t p\n" << endl;
    
    printf ("cs = %lf, Ecm = %f, b = %s, t = %s\n",
	    CSvalue, Ecm, beam.c_str(), target.c_str());
  }

  int Z1 = NuF->GetZ(beam);
  int Z2 = NuF->GetZ(target);
  double m1 = NuF->GetMass(beam, "MeV/c^2");
  double m2 = NuF->GetMass(target, "MeV/c^2");
  double mu = m1*m2/(m1+m2);
  double Sommerfeld = alpha*Z1*Z2*sqrt(mu/2/Ecm);
  double SF = exp(2*pi*Sommerfeld)*CSvalue*Ecm;

  if (!quiet) {
    printf ("Calc: reduced mass = %lf MeV/c2, Sommerfeld par = %lf, SF = %e MeV*barn\n", mu, 
	    Sommerfeld, SF);
  }
  else
    cout << SF << endl;
  return 0;
}
