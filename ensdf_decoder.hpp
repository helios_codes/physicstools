// Code: ensdf_decoder.hpp
//
// Description: 
//
// Compile with:
//
// By Daniel Santiago-Gonzalez
// Mar/2019
#ifndef ensdf_decoder_hpp_INCLUDED
#define ensdf_decoder_hpp_INCLUDED

#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

class ensdf_decoder {

public: 
  ensdf_decoder();
  int loadDatabase(std::string file);
  //  int findNuclide(std::string nuclide);
  float getL1E() {return l1E;};
  float getL1T() {return l1Tsec;};
  int getL1J() {return l1J;};
  int getL1Pi() {return l1Pi;};
  int isL1Stable() {return l1Stable;};
  int dumpStableNuclei(std::string file);

private:

  // Data base
  std::string* ensdfLine;
  long int numLines;
  int goodDataFile;

  // Basic physical quantities level 1 (ground state)
  float l1J;
  float l1E;
  int l1Pi;
  int l1Stable;
  float l1T;
  float l1Tunit;
  float l1Tsec;

  // Basic physical quantities level 2 (1st exc. state)
  float l2J;
  float l2E;
  int l2Pi;
  int l2Stable;
  float l2T;
  float l2Tunit;
  float l2Tsec;

};

#endif
