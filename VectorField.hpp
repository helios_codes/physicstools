
#ifndef VectorField_hpp_INCLUDED   
#define VectorField_hpp_INCLUDED   

// C and C++ libraries
#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <string.h>

// ROOT libraries
#include <TCanvas.h>
#include <TEveArrow.h>
#include <TEveManager.h>
#include <TEvePointSet.h>
#include <TMath.h>
#include <TStopwatch.h>

class VectorField{
public:
  VectorField(string File);      // Constructor for a vector field specified by a text file.
  VectorField(double Vx, double Vy, double Vz);  // Constructor for a constant vector field.
  //~VectorField();  // Destructor
  void AverageFieldInRepeatedPoints();
  void CreateFieldArrows(double Norm);
  void DeleteFieldArrows();
  void DrawField(TEveManager* gEve, short Color=632/*red*/, short Level=0);
  void DrawInterpolationCell(TEveManager* gEve, short Color=416/*bright green*/, short Style=9);
  void GetFieldComp(double x, double y, double z, double& Vx, double& Vy, double& Vz);
  void Interpolate();
  void SetNearPointsRegion(double DeltaRho, double Delta);

private:
  void CreateArrays(int NPoints);
  void DeleteArrays();
  void PrintPoint(int index);

  double DeltaRho;
  double DeltaZ;
  TEveArrow** FieldArrow;
  bool GoodDataFile;
  TEvePointSet* ICell;
  int* ICellPointIndex;
  int NPoints;
  double* rho;
  double* phi;
  double* x;
  double* y;
  double* z;
  double* Vrho;
  double* Vphi;
  double* Vx;
  double* Vy;
  double* Vz;
  bool* NearPoint;
  int* NearPointIndex;
  int NearPoints;
  // ClassDef(VectorField, 1);
};

#endif
