// Code: beam_kinematics.cpp
//
// Description: 
// Usage example: 
//
// Compile with:
// g++ -Wall -c beam_kinematics.cpp
// g++ beam_kinematics.o NuclideFinder.so -o beam_kinematics
//
// By Daniel Santiago-Gonzalez
// 2017/05

#include <iostream>
#include <string>
#include <cmath>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "NuclideFinder.hpp"

using namespace std;

int main (int argc, char** argv)
{
  const double alpha = 1/137.035999139; // fine structure constant
  const double pi = 3.14159265358979323846;
  const double c = 29.9792458;  // Speed of light in cm/ns.

  int opt;
  int TgtIndx = -1;
  string beam = "";
  string target = "";
  string light = "";
  string heavy = "";
  float Kb = 0;
  float Pres = 0;
  float Temp = 0;

  NuclideFinder* NuF = new NuclideFinder();


  cout << "================================================================================" << endl;
  cout << "|--- BEAM KINEMATICS ----------------------------------------------------------|" << endl; 
  cout << "| Written by Daniel Santiago-Gonzalez                                          |" << endl;
  cout << "| ver 1.0 (2017/5)                                                             |" << endl;
  cout << "| To get the latest version type:                                              |" << endl;
  cout << "| git clone https://dasago@bitbucket.org/helios_codes/physicstools.git         |" << endl;
  cout << "================================================================================" << endl;


  opterr = 0;
  while ((opt = getopt (argc, argv, "b:t:l:h:E:i:P:T")) != -1)
    switch (opt) {
    case 'b': // beam
      beam = optarg;
      break;
    case 't': // target
      target = optarg;
      break;
    case 'l': // light particle
      light = optarg;
      break;
    case 'h': // heavy particle
      heavy = optarg;
      break;
    case 'E': // beam energy in the lab
      Kb = atof(optarg);
      break;
    case 'i': // target index (gas cell, foil, etc.)
      TgtIndx = atoi(optarg);
      break;
    case 'P': // gas cell pressure [Pa]
      Pres = atof(optarg);
      break;
    case 'T': // gas cell temperature [K]
      Temp = atof(optarg);
      break;
    default:
      fprintf(stderr, "Invalid options\n");
      abort ();
    }

  
  int Z1 = NuF->GetZ(beam);
  int Z2 = NuF->GetZ(target);
  double m1 = NuF->GetMass(beam, "MeV/c^2");
  double m2 = NuF->GetMass(target, "MeV/c^2");
  if (beam!="") {
    if (target!="" && light!="" && heavy!="") 
      cout << "Reaction: "<< beam << "(" << target << "," << light << ")" << heavy << endl;
    else
      cout << "Unreacted beam: "<< beam << endl;
    cout << "Beam energy: " << Kb << " MeV" << endl;
  }
  
  if (TgtIndx<=0 || TgtIndx>3) {
    fprintf(stderr, 
	    "Invalid target index! Valid options are:\n1 - 2H gas\n2 - 3He gas\n3 - 9Be foil\n");
    abort ();
  }

  
  // Gas cell specifications from Harss et al. Review of Scientific Instruments 71, 380 (2000)
  // doi: 10.1063/1.1150211
  // Length = 35 mm
  // Windows: Havar 1.9 mg/cm2
  

  return 0;
}
