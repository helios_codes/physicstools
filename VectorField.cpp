
// The header file
#include "VectorField.hpp"

using namespace std;

///////////////////////////////////////////////////////////////////////////////////
// Constructor. Reads the data file and creates an array of TEveArrow objects 
// equal to the number of points in the file.  A file with the columns
//   x y z Vx Vy Vz
// or in cylindrical coordinates (default, but xyz mode is not yet implemented)
//   rho phi z Vrho Vphi Vz
// is expected. The 1st line is for coulumn description.
///////////////////////////////////////////////////////////////////////////////////
VectorField::VectorField(string File)
{
  string line;
  ifstream Read;
  double phi_deg;
  
  // Zero selected pointers.
  rho = 0;
  phi = 0;
  x = 0;
  y = 0;
  z = 0;
  Vrho = 0;
  Vphi = 0;
  Vx = 0;
  Vy = 0;
  Vz = 0;


  // Initialize class members
  DeltaRho = 5.0;
  DeltaZ = 5.0;
  GoodDataFile = 0;
  ICell = new TEvePointSet(9);
  ICell->SetName("Interpolation_cell");
  ICellPointIndex = new int[8];
  for (int p=0; p<8; p++)
    ICellPointIndex[p] = -1;
  NPoints = 0;
  NearPoints = 0;

  // Attempt to open the file.
  Read.open(File.c_str());
  if(!Read.is_open()) 
    cout << "*** VectorField Error: File " << File << " was not found." << endl;
  else {
    // The file is now open. Read it and count the lines.
    GoodDataFile = 1;
    do{
      getline(Read, line);
      // Only count non-empty lines.
      if (!line.empty())          
	NPoints++; 
    } while (!Read.eof());
    Read.close();
    NPoints--;        // The first line is for column description, hence the '--'.
    // Create the arrays for each point.
    CreateArrays(NPoints);
    NearPoint = new bool[NPoints];
    NearPointIndex = new int[NPoints];
    // Reopen the file to read and save the data in the arrays.
    Read.open(File.c_str());
    // First line is for colum description.
    getline(Read, line);
    for (int n=0; n<NPoints; n++) {
      Read >> rho[n] >> phi_deg >> z[n] >> Vrho[n] >> Vphi[n] >> Vz[n];
      //      cout << n << " " << rho[n] << " " << phi_deg << " " << z[n] << " " << Vrho[n] << " " << Vphi[n] << " " << Vz[n] << endl;
      NearPoint[n] = 0;
      NearPointIndex[n] = -1;
      phi[n] = phi_deg*TMath::Pi()/180.0;
      x[n] = rho[n]*cos(phi[n]);
      y[n] = rho[n]*sin(phi[n]);
      Vx[n] = Vrho[n]*cos(phi[n]) - Vphi[n]*sin(phi[n]);
      Vy[n] = Vrho[n]*sin(phi[n]) + Vphi[n]*cos(phi[n]);
    }
    Read.close();
    cout << "Vector field file \"" << File << "\" read with " << NPoints << " points" << endl;
  }
}

///////////////////////////////////////////////////////////////////////////////////
// Constructor for a constant vector field. 
///////////////////////////////////////////////////////////////////////////////////
VectorField::VectorField(double Vx, double Vy, double Vz)
{
  // Zero selected pointers.
  rho = 0;
  phi = 0;
  x = 0;
  y = 0;
  z = 0;
  Vrho = 0;
  Vphi = 0;
  this->Vx = 0;
  this->Vy = 0;
  this->Vz = 0;
  // In this case the vector field is specified only by 1 point.
  NPoints = 1;
  CreateArrays(NPoints);
  this->Vx[0] = Vx;
  this->Vy[0] = Vy;
  this->Vz[0] = Vz;

}

///////////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////////
void VectorField::AverageFieldInRepeatedPoints()
{
  int New_NPoints=NPoints, NRP=0, TotRP=0;
  bool skip_point;

  // The elements of this array tell whether or not to delete a point. By decreasing
  // the number of points in the main coordinate and field component arrays the sorting
  // times will be reduced.
  bool* ErasePoint = new bool[NPoints];

 // Repeated points
  int* RP_freq = new int[NPoints]; 
  double* RP_x = new double[NPoints];
  double* RP_y = new double[NPoints];
  double* RP_z = new double[NPoints];

  // Average field
  double* A_Vx = new double[NPoints];
  double* A_Vy = new double[NPoints];
  double* A_Vz = new double[NPoints];

  // Initialize arrays
  for (int i=0; i<NPoints; i++) {
    ErasePoint[i] = 0;
    RP_freq[i] = 0;
    RP_x[i] = RP_y[i] = 0;
    RP_z[i] = -10000; // Send the points far away so that there is no overlap with the real points.
    A_Vx[i] = A_Vy[i] = A_Vz[i] = 0;
  }

  // Main for-loop to look for repeated points and to average the field components of those points.
  for (int i1=0; i1<NPoints-1; i1++) {
    // We start by assuming this point has not been repeated.
    skip_point = 0;
    for (int iNRP=0; iNRP<NRP; iNRP++) {
      // Has point 1 already been reported as a repeated point?
      if (x[i1]==RP_x[iNRP] && y[i1]==RP_y[iNRP] && z[i1]==RP_z[iNRP]) {
	skip_point = 1;
	//cout << "Point skipped at i1=" << i1 << endl;
      }
    }
    // We look for repeated points provided that the current reference point (i1) has not been
    // identified as a repeated point, otherwise we skip to the next value of i1.
    if (!skip_point) {
      for (int i2=i1+1; i2<NPoints; i2++) {
	// Is the position of point 2 equal to the one of point 1?
	if (x[i1]==x[i2] && y[i1]==y[i2] && z[i1]==z[i2]) {
	  // Is this a new repeated number (i.e. is this the first repetition of the ref. point)?
	  if (RP_freq[NRP]==0) {
	    RP_freq[NRP] = 2;
	    RP_x[NRP] = x[i1];
	    RP_y[NRP] = y[i1];
	    RP_z[NRP] = z[i1];
	    A_Vx[NRP] = Vx[i1] + Vx[i2];
	    A_Vy[NRP] = Vy[i1] + Vy[i2];
	    A_Vz[NRP] = Vz[i1] + Vz[i2];
	    /*
	    cout << NRP << " RP i1=" << i1 << " (" << Vrho[i1] << "," << Vphi[i1] << "," << Vz[i1]
		 << ")" << " i2=" << i2 << " (" << Vrho[i2] << "," << Vphi[i2] << "," << Vz[i2] 
		 << ") AV=(" << A_Vx[NRP]/2 << "," << A_Vz[NRP]/2 << "," << A_Vz[NRP]/2 << ")" << endl;
	    */
	  }
	  // If not, this number has been repeated before in this cycle.  Increment the frequency
	  // by one and increment the vector components by the values at this point (i2).
	  else {
	    RP_freq[NRP]++;
	    A_Vx[NRP] += Vx[i2];
	    A_Vy[NRP] += Vy[i2];
	    A_Vz[NRP] += Vz[i2];
	    /*
	    cout << NRP << " RP i=" << i2 << " (" << Vrho[i2] << "," << Vphi[i2] << "," << Vz[i2] 
		 << ") AV=(" << A_Vx[NRP]/RP_freq[NRP] << "," << A_Vz[NRP]/RP_freq[NRP] 
		 << "," << A_Vz[NRP]/RP_freq[NRP] << ") Freq=" << RP_freq[NRP] << endl;
	    */
	  }
	  // Since this is a repeated point it can be deleted.
	  ErasePoint[i2] = 1;
	}
      } // end for(i2)
      // If at the end of the cycle the frequency of the current repeated point is greater than
      // zero average the components and then increment the number of repeated points by one,
      // hence creating space for a new repeated number.
      if (RP_freq[NRP]>0) {
	A_Vx[NRP] /= RP_freq[NRP];
	A_Vy[NRP] /= RP_freq[NRP];
	A_Vz[NRP] /= RP_freq[NRP];
	Vx[i1] = A_Vx[NRP];
	Vy[i1] = A_Vy[NRP];
	Vz[i1] = A_Vz[NRP];
	Vrho[i1] = Vx[i1]*cos(phi[i1]) + Vy[i1]*sin(phi[i1]);
	Vphi[i1] = -Vx[i1]*sin(phi[i1]) + Vy[i1]*cos(phi[i1]);
	NRP++;
      }
    } // end if(skip_point)  
  } // end for(i1)
  
  for (int i=0; i<NRP; i++)
    TotRP += RP_freq[i];
  TotRP -= NRP;
  cout << "Number of repeated points = " << TotRP << endl;
  New_NPoints = NPoints - TotRP;
  
  double* New_rho = new double[New_NPoints];
  double* New_phi = new double[New_NPoints];
  double* New_x = new double[New_NPoints];
  double* New_y = new double[New_NPoints];
  double* New_z = new double[New_NPoints];
  double* New_Vrho = new double[New_NPoints];
  double* New_Vphi = new double[New_NPoints];
  double* New_Vx = new double[New_NPoints];
  double* New_Vy = new double[New_NPoints];
  double* New_Vz = new double[New_NPoints];

  // Save all the information from the points that are not repeated and the ones whose
  // field components have been averaged in the 'New' arrays.
  int k=0;
  for (int i=0; i<NPoints; i++)
    if (!ErasePoint[i] && k<New_NPoints) {
      New_rho[k] = rho[i];
      New_phi[k] = phi[i];
      New_x[k] = x[i];
      New_y[k] = y[i];
      New_z[k] = z[i];
      New_Vrho[k] = Vrho[i];
      New_Vphi[k] = Vphi[i];
      New_Vx[k] = Vx[i];
      New_Vy[k] = Vy[i];
      New_Vz[k] = Vz[i];
      k++;
    }

  // Resize the coordinate and field components arrays.
  CreateArrays(New_NPoints);

  // Pass the coordinates and field components from the 'New' arrays to the standar arrays
  // used in this class.
  for (int i=0; i<New_NPoints; i++) {
    rho[i] = New_rho[i];
    phi[i] = New_phi[i];
    x[i] = New_x[i];
    y[i] = New_y[i];
    z[i] = New_z[i];
    Vrho[i] = New_Vrho[i];
    Vphi[i] = New_Vphi[i];
    Vx[i] = New_Vx[i];
    Vy[i] = New_Vy[i];
    Vz[i] = New_Vz[i];
  }
  // Finally, change the number of points.
  NPoints = New_NPoints;
  cout << "New size of arrays (number of points) = " << NPoints << endl;

  // Delete all the arrays created in this function.
  delete[] RP_freq;
  delete[] RP_x;
  delete[] RP_y;
  delete[] RP_z;
  delete[] A_Vx;
  delete[] A_Vy;
  delete[] A_Vz;
  delete[] New_rho;
  delete[] New_phi;
  delete[] New_x;
  delete[] New_y;
  delete[] New_z;
  delete[] New_Vrho;
  delete[] New_Vphi;
  delete[] New_Vx;
  delete[] New_Vy;
  delete[] New_Vz;
  return;
}



void VectorField::CreateArrays(int Dim)
{
  DeleteArrays();
  rho = new double[Dim];
  phi = new double[Dim];
  x = new double[Dim];
  y = new double[Dim];
  z = new double[Dim];
  Vrho = new double[Dim];
  Vphi = new double[Dim];
  Vx = new double[Dim];
  Vy = new double[Dim];
  Vz = new double[Dim];
  return;
}


///////////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////////
void VectorField::CreateFieldArrows(double Norm)
{
  //  DeleteFieldArrows();
  FieldArrow = new TEveArrow*[NPoints];
  for (int n=0; n<NPoints; n++) 
    FieldArrow[n] = new TEveArrow(Norm*Vx[n], Norm*Vy[n], Norm*Vz[n], x[n], y[n], z[n]);
  return;
}


///////////////////////////////////////////////////////////////////////////////////
// Delete the arrays of coordiantes and vector components if the have been created.
// I have seen that if the pointers are not initialized (to zero) in the 
// constructor, the delete operator creates problem.
///////////////////////////////////////////////////////////////////////////////////
void VectorField::DeleteArrays()
{ 
  if (rho!=0) {
    delete[] rho;
    rho = 0;
  }
  if (phi!=0) {
    delete[] phi;
    phi = 0;
  }
  if (x!=0) {
    delete[] x;
    x = 0;
  }
  if (y!=0) {
    delete[] y;
    y = 0;
  }
  if (z!=0) {
    delete[] z;
    z = 0;
  }
  if (Vrho!=0) {
    delete[] Vrho;
    Vrho = 0;
  }
  if (Vphi!=0) { 
    delete[] Vphi;
    Vphi = 0;
  }
  if (Vx!=0) {
    delete[] Vx;
    Vx = 0;
  }
  if (Vy!=0) {
    delete[] Vy;
    Vy = 0;
  }
  if (Vz!=0) {
    delete[] Vz;
    Vz = 0;
  }
  return;
}


///////////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////////
void VectorField::DeleteFieldArrows()
{
  if (FieldArrow!=0) {
    for (int n=0; n<NPoints; n++) 
      delete FieldArrow[n];
    delete[] FieldArrow;
    FieldArrow = 0;
  }
  return;
}


///////////////////////////////////////////////////////////////////////////////////
// Transparency level from 0 to 100.
// It seems that the SetX methods of the TEveElements need an existing TEveManager.
// Otherwise it gives a segment violation.
///////////////////////////////////////////////////////////////////////////////////
void VectorField::DrawField(TEveManager* gEve, short Color, short Level)
{
  if (FieldArrow!=0) {
    for (int n=0; n<NPoints; n++) {
      if (NearPoint[n]) {
	FieldArrow[n]->SetMainColor(kBlue);
	FieldArrow[n]->SetMainTransparency(0);
      }
      else {
	FieldArrow[n]->SetMainColor(Color);
	FieldArrow[n]->SetMainTransparency((Char_t)Level);
      }
      gEve->AddElement(FieldArrow[n]);
    }
    gEve->FullRedraw3D(kTRUE);
  }
  else
    cout << "ERROR: Field arrows have not been created." << endl;
  return;
}


///////////////////////////////////////////////////////////////////////////////////
// The interpolation cell (ICell) is created in the contructor.
///////////////////////////////////////////////////////////////////////////////////
void VectorField::DrawInterpolationCell(TEveManager* gEve, short Color, short Style)
{
  int i;
  ICell->SetMarkerColor(Color);
  ICell->SetMarkerStyle(Style);
  for (int p=0; p<8; p++) {
    i = ICellPointIndex[p];
    if (i>=0)
      ICell->SetPoint(p+1, x[i], y[i], z[i]);
    else
      cout << "Warning: Invalid index of interpolation cell point " << p << endl;
  }
  gEve->AddElement(ICell);
  gEve->FullRedraw3D(kTRUE);
  return;
}


///////////////////////////////////////////////////////////////////////////////////
// For a given point get the cartesian field components.
// Indices of the points forming the up-stream and down-stream faces of the 
// interpolation cell:
//        US            DS
//    3--------2    7--------6
//     \ *    /      \ *    /
//      \    /        \    /
//       1--0          5--4
// The '*' marks the projection of the point (X,Y,Z) where the field will be interpolated
// onto the up-stream and down-stream faces.
///////////////////////////////////////////////////////////////////////////////////
void VectorField::GetFieldComp(double X, double Y, double Z, double& VX, double& VY, double& VZ)
{
  double pi = TMath::Pi();
  double Dist=0, DistClosePoint1=1000, DistClosePoint2=1000, Rho=0, Phi=0;
  int IndexClosePoint1=-1, IndexClosePoint2=-1; 
  int iDSCP[4] = {-1,-1,-1,-1}; // indices of down-stream close points.
  int iUSCP[4] = {-1,-1,-1,-1}; // indices of up-stream close points.
  double DistDSCP[4]={1000,1000,1000,1000};
  double DistUSCP[4]={1000,1000,1000,1000};
  double ZCompCrossProd = 0;
  TStopwatch ProcessTime;

  if (NPoints>1) {
    ICell->SetPoint(0, X, Y, Z);
    // Reset indices of points forming the interpolation cell.
    for (int p=0; p<8; p++)
      ICellPointIndex[p] = -1;

    // Start the loop stopwatch to see how long it took to find the close points for interpolation.
    ProcessTime.Start();
    for (int i=0; i<NPoints; i++) {
      Dist = sqrt(pow(X-x[i],2) + pow(Y-y[i],2) + pow(Z-z[i],2));
      Rho = sqrt(X*X + Y*Y);
      // Phi must be an element of [0,2pi) since this is the domain of phi[].
      if (X<0)
	Phi = pi + atan(Y/X);     // For quadrants II and III.
      else {
	if (Y>=0)
	  Phi = atan(Y/X);        // For quadrant I.
	else
	  Phi = 2*pi + atan(Y/X); // For quadrant IV.  The atan() is negative.
      }
      ZCompCrossProd = (X*y[i] - x[i]*Y);
      // Up-stream case
      if (z[i]<Z) {
	// For US points 0 and 1
	if (rho[i]<Rho) {
	  // Is it point 0 or 1?  The z-component of the cross product can tell us this.
	  if (ZCompCrossProd<0) { // Case for point 0
	    if (Dist<DistUSCP[0]) {
	      ICellPointIndex[0] = iUSCP[0] = i;
	      DistUSCP[0] = Dist;
	    }
	  }
	  else {                  // Case for point 1
	    if (Dist<DistUSCP[1]) {
	      ICellPointIndex[1] = iUSCP[1] = i;
	      DistUSCP[1] = Dist;
	    }
	  }	   
	}
	// For points 2 and 3
	else {
	  // Is it point 2 or 3?  The z-component of the cross product can tell us this.
	  if (ZCompCrossProd<0) { // Case for point 2
	    if (Dist<DistUSCP[2]) {
	      ICellPointIndex[2] = iUSCP[2] = i;
	      DistUSCP[2] = Dist;
	    }
	  }
	  else {                  // Case for point 3
	    if (Dist<DistUSCP[3]) {
	      ICellPointIndex[3] = iUSCP[3] = i;
	      DistUSCP[3] = Dist;
	    }
	  }	   
	}
      }
      // Down-stream case
      else {
	// For DS points 0 and 1
	if (rho[i]<Rho) {
	  // Is it point 0 or 1?  The z-component of the cross product can tell us this.
	  if (ZCompCrossProd<0) { // Case for point 0
	    if (Dist<DistDSCP[0]) {
	      ICellPointIndex[4] = iDSCP[0] = i;
	      DistDSCP[0] = Dist;
	    }
	  }
	  else {                  // Case for point 1
	    if (Dist<DistDSCP[1]) {
	      ICellPointIndex[5] = iDSCP[1] = i;
	      DistDSCP[1] = Dist;
	    }
	  }	   
	}
	// For points 2 and 3
	else {
	  // Is it point 2 or 3?  The z-component of the cross product can tell us this.
	  if (ZCompCrossProd<0) { // Case for point 2
	    if (Dist<DistDSCP[2]) {
	      ICellPointIndex[6] = iDSCP[2] = i;
	      DistDSCP[2] = Dist;
	    }
	  }
	  else {                  // Case for point 3
	    if (Dist<DistDSCP[3]) {
	      ICellPointIndex[7] = iDSCP[3] = i;
	      DistDSCP[3] = Dist;
	    }
	  }	   
	}
      } 
    } 
  
    ProcessTime.Stop();
    cout << "Processing time:\n real = " << ProcessTime.RealTime() << " s\n" 
	 << " CPU = " << ProcessTime.CpuTime()*1000.0 << " s" << endl;
    ProcessTime.Print("u");
    cout << "up-stream points" << endl;
    PrintPoint(iUSCP[0]);
    PrintPoint(iUSCP[1]);
    PrintPoint(iUSCP[2]);
    PrintPoint(iUSCP[3]);
    cout << "dn-stream points" << endl;
    PrintPoint(iDSCP[0]);
    PrintPoint(iDSCP[1]);
    PrintPoint(iDSCP[2]);
    PrintPoint(iDSCP[3]);
  }
  else {
    VX = Vx[0];
    VY = Vy[0];
    VZ = Vz[0];
  }
  return;
}

///////////////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////////////
void VectorField::SetNearPointsRegion(double DeltaRho, double DeltaZ)
{
  this->DeltaRho = DeltaRho;
  this->DeltaZ = DeltaZ;
  cout << "DR=" << DeltaRho << "  DZ=" << DeltaZ << endl;
  return;
}


void VectorField::PrintPoint(int index)
{
  int i = index;
  if (i>=0 && i<NPoints)
    cout << i << "  r=("<< x[i] << "," << y[i] << "," << z[i] << ") \t V=("
	 << Vx[i] << "," << Vy[i] << "," << Vz[i] << ")" << endl; 
  return;
}


// http://arxiv.org/pdf/1102.4852v1.pdf
void VectorField::Interpolate()
{

  return;
}
