// Code: SFtoCS.cpp
//
// Description: Calculates the cross section for the astrophysical S-factor.
// Usage example: Say you want to get the cross section for the 17O(p,gamma) reaction and you know 
//                that the S-factor is 1e10 MeV*barn and that the center-of-mass energy is 0.5 MeV
// SFtoCS -s 1e10 -e 0.5 -b 17O -t p
//
// Compile with:
// g++ -Wall -c SFtoCS.cpp
// g++ SFtoCS.o NuclideFinder.so -o SFtoCS
//
// By Daniel Santiago-Gonzalez
// Aug/2016

#include <iostream>
#include <string>
#include <cmath>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "NuclideFinder.hpp"

using namespace std;

int main (int argc, char** argv)
{
  cout << " Program SFtoCS\n"
       << " by Daniel SG, Aug/2016\n"
       << " Description: Converts astrophysical S-factor to cross section.\n"
       << " Usage example: Say you want the cross section for the 17O(p,gamma) reaction and you\n"
       << " know that the S-factor is 27.6 MeV*barn at a center-of-mass energy of 0.5 MeV, type\n"
       << " SFtoCS -s 27.6 -e 0.5 -b 17O -t p\n" << endl;
  const double alpha = 1/137.035999139; // fine structure constant
  const double pi = 3.14159265358979323846;

  int opt;
  double SFvalue = 0;
  double Ecm = 0;
  string beam;
  string target;

  NuclideFinder* NuF = new NuclideFinder();

  opterr = 0;
  while ((opt = getopt (argc, argv, "s:e:b:t:")) != -1)
    switch (opt) {
    case 's': // S-factor
      SFvalue = atof(optarg);    
      break;
    case 'e': // center-of-mass energy
      Ecm = atof(optarg);
      break;
    case 'b': // beam
      beam = optarg;
      break;
    case 't': // target
      target = optarg;
      break;
    default:
      fprintf(stderr, "Usage: SFtoCS -s 27.6 -e 0.5 -b 17O -t p\n");
      abort ();
    }
  printf ("Input: s = %.2E MeV*b, Ecm = %.2f MeV, b = %s, t = %s\n",
          SFvalue, Ecm, beam.c_str(), target.c_str());
  
  int Z1 = NuF->GetZ(beam);
  int Z2 = NuF->GetZ(target);
  double m1 = NuF->GetMass(beam, "MeV/c^2");
  double m2 = NuF->GetMass(target, "MeV/c^2");

  double mu = m1*m2/(m1+m2);
  //  0.157;
  double Sommerfeld = alpha*Z1*Z2*sqrt(mu/2/Ecm);
  double CS = exp(-2*pi*Sommerfeld)*SFvalue/Ecm;
  printf ("Calc: reduced mass = %lf MeV/c2, Sommerfeld par = %lf\nCS = %e barn\n", mu, Sommerfeld, CS);
  /* for (index = optind; index < argc; index++) */
  /*   printf ("Non-option argument %s\n", argv[index]); */

  //  cout << 


  return 0;
}
