/*******************************************************************
Code: KinematicCurve.hpp

Description: This class gives the kinematic curves as a TGraph
  object. The masses for the reaction and the energy of excitation
  must be set before getting the curve.

Author: Daniel Santiago-Gonzalez
2013-March
*******************************************************************/

#ifndef KinematicCurve_hpp_INCLUDED   
#define KinematicCurve_hpp_INCLUDED   

#include <iostream>
#include <string>
#include <cmath>
#include <TMath.h>
#include <TGraph.h>
#include "FourVector.hpp"

class KinematicCurve: public TGraph{
public:
  KinematicCurve();
  void SetReaction(float Kb, float mb, float mt, float mh, float ml, float Eexc=0);
  void SetReaction(double Kb, double mb, double mt, double mh, double ml, double Eexc=0);
  void GetCurve(int Points, std::string YvZ="EvA");
  void GetCurveRel(int Points, std::string YvX="EvA");
  void GetCurveRelH(int Points, std::string YvX="EvA");
  void GetHELIOSCurve(int Points, double BField, double zmin, double zmax, int A, int Q);
  double GetCMKineticEnergy();

private:
  double mb, mt, mh, ml, Eexc, Kb;
  double vCM, vb, pb, ECM;
  double BetaZ;
  FourVector Pb;
  FourVector Pt;
  FourVector Pl;
  FourVector Ph;
  FourVector Ptot;

  static const double pi = 3.1415926535898;
  static const double c = 29.9792458;  // Speed of light in cm/ns.

  // Need to uncomment this when using this class directly in CINT.
  ClassDef(KinematicCurve,1);
  // I've seen that when this class is included in another class or code
  // and ACLiC is used to compile such code you don't need to uncomment the
  // line above.
};

#endif
