#!/bin/bash
for i in $(seq -f "%03g" 1 55)
do
    awk '{if(/MeV  is/) print $63}' < Untitled_step$i.rtf >> dumpCS
done

for i in $(seq -f "%03g" 1 55)
do
    awk '{if(/MeV  is/) print $130}' < Untitled_step$i.rtf >> dumpEcm
done

sed -i.bak 's/\\i/ /' dumpCS
sed -i.bak 's/\\par/ /' dumpEcm
